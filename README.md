Consider a user using borwser check his mails, here's simple sequence diagram:

```mermaid
sequenceDiagram
    Browser->>Gmail: POST gmail.com/login
    Gmail->>Browser: Set-Cookie: token=xxx
    activate Browser
    Note right of Browser: cookie[gmail.com]: {token=xxx}
    Browser->>Gmail: GET gmail.com/mails
	Note right of Browser: header: {Cookie: {token=xxx}}
    Gmail-->>Browser: [{id:1,...},{id:2,...}]
    Browser->>Gmail: DELETE gmail.com/mails?id=2
	Note right of Browser: header: {Cookie: {token=xxx}}
    Gmail-->>Browser: OK
    Browser->>Gmail: POST gmail.com/logout
    Gmail->>Browser: OK
    deactivate Browser
    Note right of Browser: cookie[gmail.com]: {}
    Browser->>Gmail: GET gmail.com/mails
	Note right of Browser: header: {Cookie: {token=xxx}}
	Gmail->>Browser: 401 Unauthorized
```

Usually the user won't logout gmail, before he continues to other website. Then this other website can launch CSRF attack like this:

```mermaid
sequenceDiagram
    Browser->>Gmail: POST gmail.com/login
    Gmail->>Browser: Set-Cookie: token=xxx
    activate Browser
    Note right of Browser: cookie[gmail.com]: {token=xxx}
	Browser->>OtherWebsite: GET http://other.com/
	activate Browser
    Note right of Browser: cookie[other.com]: {}
	OtherWebsite->>Browser: OK body: <img src="https://gmail.com/mails">
	Browser->>Gmail: GET http://gmail.com/mails
    Note right of Browser: cookie[gmail.com]: {token=xxx}
	Browser->>Browser: User click submit form
	Browser->>Gmail: POST gmail.com/mails
    Note right of Browser: cookie[gmail.com]: {token=xxx}
	Browser->>Browser: execute javascript
	Browser->>Gmail: DELETE gmail.com/mails?id=1
    Note right of Browser: cookie[gmail.com]: {token=xxx}
	deactivate Browser
    Browser->>Gmail: POST gmail.com/logout
    Gmail->>Browser: OK
    deactivate Browser
    Note right of Browser: cookie[gmail.com]: {}
```

